from flask import Flask, request, render_template
import sqlite3
import time
import numpy as np
import pandas as pd
from datetime import datetime, timedelta

app = Flask(__name__)

@app.route('/')
def hello_world():
    conn = sqlite3.connect('db.db')
    conn.execute('''
                create table if not exists users_history
                (
                    id         integer
                        constraint table_name_pk
                            primary key autoincrement,
                    ip_addr    text,
                    user_agent text,
                    dt_stamp   integer default 0
                );
    ''')
    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr

    conn.execute("INSERT INTO `users_history` (`ip_addr`, `user_agent`, `dt_stamp`) VALUES (?, ?, ?)", (ip,
                                                                                                    request.user_agent.string,
                                                                                                    int(time.time())))
    conn.commit()

    c = conn.cursor()
    c.execute("SELECT * FROM `users_history`")
    data = c.fetchall()

    df = pd.DataFrame(data=data)
    df.columns = ["id", "ip_addr", "user_agent", "dt_stamp"]
    df['dt_stamp'] = pd.to_datetime(df['dt_stamp'], unit='s')
    agg10 = df[df["dt_stamp"] > datetime.utcnow()-timedelta(hours=3)].groupby(pd.Grouper(freq='10T', key="dt_stamp")).count()
    agg10dts = agg10.to_dict()["id"]
    return render_template("index.html", data=data, stamps=agg10dts.keys(), counts=agg10dts.values())

@app.route('/hello2/<username>/<int:userage>')
def hello_world2(username, userage):
    return render_template("index.html", username=username, userage=userage)

if __name__ == '__main__':
    app.run()
